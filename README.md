# HypEventsNG

I will try to reanime the HYPEvents Project( Original Authors are Fred Beckhusen and Koen Martens) . It's collect events in opensim grids and show them on Website http:// yourworlds.eu/hypevents and on a free board in my grid: http://yourworlds.eu:8002
The Project is set under the GPL v3.

This is my bash script to run the server:


#!/bin/bash
set -eu

while true; do

echo ${PATH}
echo `pwd`

OUTPUT="/var/www/html/hypevents"
cd /root//hypevents

echo "=======> `date`"
echo "       > start fetch"

./main.py -f

now=`date +%s`
last=`expr ${now} + 2592000`
first=`expr ${now} - 86400`

echo "now ${now} last ${last} first ${first} "

echo "write json"

jsafter=`date +%Y-%m-%d --date="@$first"` 
jsbefore=`date +%Y-%m-%d --date="@$last"`

echo $jsafter
echo $jsbefore

./main.py -w -e json -o "${OUTPUT}/events.json.new" -a $jsafter -b $jsbefore

mv ${OUTPUT}/events.json.new ${OUTPUT}/events.json

echo "write lsl"

./main.py -w -e lsl -o "${OUTPUT}/events.lsl.new" -a "`date`"
./main.py -w -e lsl2 -o "${OUTPUT}/events.lsl2.new" -a "`date`"

mv ${OUTPUT}/events.lsl.new ${OUTPUT}/events.lsl
mv ${OUTPUT}/events.lsl2.new ${OUTPUT}/events.lsl2

echo "write html"

./main.py -w -e html -o "html/events.html" -a "`date`"

cd html

make

cp style.css "${OUTPUT}/style.css.new"
cp scr.js "${OUTPUT}/scr.js.new"
cp index.html "${OUTPUT}/index.html.new"

mv "${OUTPUT}/style.css.new" "${OUTPUT}/style.css"
mv "${OUTPUT}/scr.js.new" "${OUTPUT}/scr.js"
mv "${OUTPUT}/index.html.new" "${OUTPUT}/index.html"

sleep 600

done


