import requests
from lib.event import Event
from fetcher.icalfetcher import IcalFetcher
import icalendar
from lib.category import Category
from fetcher.icalfetcher import IcalFetcher
from helper.yourworlds import yourworldsHelper
from datetime import timedelta

class yourworldsfetcher(IcalFetcher):
    def __init__(self, eventlist, webcache=None):
        super(yourworldsfetcher,self).__init__(
            "http://yourworlds.eu/createCal/calendar.ics",
            [ Category("events") ],
            eventlist,
            webcache, 
            yourworldsHelper()
        )
        print("ReadCal from yourworlds")
        self.webcache = webcache
        self.minexpiry = 1000
        self.maxexpiry = 1800

if __name__=='__main__':
    from lib.webcache import WebCache
    from lib.eventlist import EventList

    eventlist = EventList()
    #print("Eventlist: 1 ")
    cache = WebCache('data/test_yourworldsfetcher.pck')
    #print("Eventlist:2 ")
    f = yourworldsfetcher(eventlist, cache)
    #print("Eventlist:3 ")
    f.fetch()
    #print("Eventlist: 4")
    cache.flush()
    #print("Eventlist: ")
    for ev in eventlist:
        print str(ev)
    #print("End Eventlist: ")
