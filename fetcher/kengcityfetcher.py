import requests
from lib.event import Event
from fetcher.icalfetcher import IcalFetcher
import icalendar
from lib.category import Category
from helper.kengcity import KengCityHelper

class KengCityFetcher(IcalFetcher):
    def __init__(self, eventlist, webcache=None):

        super(KengCityFetcher,self).__init__(
            "https://calendar.google.com/calendar/ical/antoinnettekeng%40gmail.com/public/basic.ics",
            [],
            eventlist,
            webcache,
            KengCityHelper()
        )
        
        ##super(KengCityFetcher,self).__init__(
        #  "https://calendar.google.com/calendar?cid=YW50b2lubmV0dGVrZW5nQGdtYWlsLmNvbQ",
        #    [],
         #   eventlist,
         #   webcache,
          #  KengCityHelper()
         ## )
        
        self.webcache = webcache
        self.minexpiry = 1000
        self.maxexpirty = 1800

if __name__=='__main__':
    from lib.webcache import WebCache
    from lib.eventlist import EventList

    eventlist = EventList()

    cache = WebCache("data/test_kengcity.cache")

    f = KengCityFetcher(eventlist, cache)

    e = f.fetch()

    cache.flush()

    for ev in eventlist:
        print str(ev)
